﻿using System.IO;
using System.Security.AccessControl;

namespace Streamer_UI.Classes
{
    public static class LogsGenerator 
    {
        private static DirectorySecurity directorySecurity = new DirectorySecurity();
        public static void CreateandValidateDirectory()
        {
            Directory.CreateDirectory(MasterTagVariables.HomeFolderPath);
            Directory.CreateDirectory(MasterTagVariables.LogsRecordFolderPath);
            Directory.CreateDirectory(MasterTagVariables.MediaLibDataFolderPath);
        }
        public static void CreateandValidateFiles()
        {
            if(!File.Exists(MasterTagVariables.HomeFolderPath + MasterTagVariables.mediaDbFileName))
            {
                File.Create(MasterTagVariables.MediaLibDataFolderPath + MasterTagVariables.mediaDbFileName);
            }
        }
    }
}