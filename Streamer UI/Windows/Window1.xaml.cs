﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MaterialDesignColors;
using MaterialDesignThemes.Wpf;
namespace Streamer_UI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnCloseMenu.Visibility = Visibility.Hidden;
            btnOpenManu.Visibility = Visibility.Visible;

            SidePanel.Width = 250;
        }

        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnOpenManu.Visibility = Visibility.Hidden;
            btnCloseMenu.Visibility = Visibility.Visible;
            SidePanel.Width = 80;
        }
    }
}
