﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Streamer_UI
{
    /// <summary>
    /// Interaction logic for Window3.xaml
    /// </summary>
    public partial class Window3 : Window
    {
        double PlayButtonSize;
        public Window3()
        {
            InitializeComponent();
            //controlGrid = GetTemplateChild("PART_ControlGrid") as Grid;
            //controlGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            //controlGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            //controlGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            //controlGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            //controlGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            //controlGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            //controlGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            //controlGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            //controlGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

            //playButton = CreatePlayButton();
            //Grid.SetColumn(playButton, 0);
            //controlGrid.Children.Add(playButton);
        }

        private Path CreatePlayButton()
        {
            Path p = new Path();

            p.StrokeThickness = 0.0;
            p.Fill = Brushes.Gray;

            p.HorizontalAlignment = HorizontalAlignment.Center;
            p.VerticalAlignment = VerticalAlignment.Center;

            Point start = new Point(PlayButtonSize * 0.8, PlayButtonSize / 2.0);
            LineSegment[] segments = new[]
            {
        new LineSegment(new Point(0, 0), true),
        new LineSegment(new Point(0, PlayButtonSize), true)
    };
            PathFigure figure = new PathFigure(start, segments, true);
            PathGeometry geometry = new PathGeometry(new[] { figure });

            p.Margin = new Thickness(16.0, 0, 0, 8.0);
            p.Data = geometry;

            p.MouseEnter += (s, e) => { p.Fill = Brushes.LightGray; };
            p.MouseLeave += (s, e) => { p.Fill = Brushes.Gray; };

            return p;
        }

    }
}
