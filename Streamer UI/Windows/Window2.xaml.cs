﻿using Streamer_UI.Classes;
using Streamer_UI.UserControls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Streamer_UI
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {

        //
        //
        //External Song info class object
        SongInfo GetSongInfo = new SongInfo();
        //
        //A Class Type List that is used For Binding data to DataGrid
        private static List<SongInfo> songList = new List<SongInfo>();
        //
        //This Random Class Used For Choosing a random song in loading music files
        Random rndmPositonSelection = new Random();
        //
        //This is the media controller that is used for play songs
        MediaPlayer player = new MediaPlayer();
        //
        //This is being used...................next progess
        public delegate void TimerTick();
        TimerTick songTick;
        //
        //This is timer class which is used for Calculating timing
        DispatcherTimer timer = new DispatcherTimer();
        //
        //=================================================
        #region Window loading 
        public Window2()
        {
            InitializeComponent();
            LogsGenerator.CreateandValidateDirectory();
            LogsGenerator.CreateandValidateFiles();
            player.MediaEnded += OnMediaEnded;
            player.IsMuted = true;
        }
        private async void FormLoad(object sender, RoutedEventArgs e)
        {
            volumeSlider.Value = 50;
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            loaderControl.Visibility = Visibility.Visible;
            // Try to create the directory.
            DirectoryInfo di = Directory.CreateDirectory(@"E:\Music");
            // run a method in another thread
            await Task.Run(() => FileSearchAlgorithm(di));
            gridLoader.ItemsSource = songList;
            loaderControl.Visibility = Visibility.Hidden;
            if (gridLoader.HasItems)
            {
                gridLoader.SelectedItem = gridLoader.Items[0];
                gridLoader.Focus();
            }
        }
        #endregion

        #region Media player control events
        private void OnMediaEnded(object sender, EventArgs e)
        {
            playRandomMusic();
        } 
        #endregion

        #region Get Music duration and recursivly increment till the song end
        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (player.Source != null)
                {
                    //SeekToMediaPosition.Value =Convert.ToDouble(player.Position);
                    lblStatus.Content = String.Format("{0} / {1}", player.Position.ToString(@"mm\:ss"), player.NaturalDuration.TimeSpan.ToString(@"mm\:ss"));
                }
                else
                {
                    lblStatus.Content = "No file selected...";
                }
            }
            catch (Exception)
            {
                lblStatus.Content = "No file selected...";
            }
        } 
        #endregion

        #region Search Algo For Selecting Music recursively from directory 
        public static void FileSearchAlgorithm(DirectoryInfo baseDirectory)
        {
            try
            {
                int count = Directory.EnumerateFiles(@"E:\Music", "*.mp3", SearchOption.AllDirectories).Count();
                foreach (DirectoryInfo dirInfo in baseDirectory.GetDirectories())
                {
                    foreach (FileInfo f in dirInfo.GetFiles("*.mp3"))
                    {
                            TagLib.File tagFile = TagLib.File.Create(f.FullName);
                            //string artist = tagFile.Tag.FirstAlbumArtist;
                            //string album = tagFile.Tag.Album;
                            //string title = tagFile.Tag.Title;
                            //uint beatsPerMinute = tagFile.Tag.BeatsPerMinute;
                            //String lyrics = tagFile.Tag.Lyrics;
                            //String sortTitle = tagFile.Tag.TitleSort;
                            //String bitRate = tagFile.Properties.AudioBitrate + " kbps";
                            String duration = tagFile.Properties.Duration.ToString(@"mm\:ss");
                            songList.Add(new SongInfo() { songName = f.Name, songPath = f.FullName, songDirectory = f.DirectoryName, songLength = duration });
                    }
                    FileSearchAlgorithm(dirInfo);
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
        }
        #endregion

        #region Methods For Selecting in loaded list
        private void playCurrentSong()
        {
            try
            {
                GetSongInfo = (SongInfo)gridLoader.SelectedItem;
                timer.Stop();
                player.Open(new Uri(GetSongInfo.songPath, UriKind.Relative));
                player.Play();
                timer.Start();
                SeekToMediaPosition.Maximum = Convert.ToDouble(player.NaturalDuration.TimeSpan);
            }
            catch (Exception)
            {

            }
        }
        private void playRandomMusic()
        {
            if (gridLoader.Items.Count > 0)
            {
                gridLoader.SelectedIndex = rndmPositonSelection.Next(gridLoader.Items.Count - 1);
                try
                {
                    gridLoader.ScrollIntoView(gridLoader.SelectedItem);
                    playCurrentSong();
                }
                catch (IndexOutOfRangeException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        } 
        #endregion

        #region DataGrid Row Double Click 
        private void GridLoaderRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            playCurrentSong();
        } 
        #endregion

        #region Buttons Clicks For pervious and next song
        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            timer.Start();
            player.Play();
        }
        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            player.Pause();
        }
        private void prevSong_Click(object sender, RoutedEventArgs e)
        {
            previousSong();
        }
        private void previousSong()
        {
            gridLoader.Focus();
            if (gridLoader.SelectedIndex - 1 < 0)
            {
                return;
            }
            gridLoader.SelectedIndex = gridLoader.SelectedIndex - 1;
            gridLoader.ScrollIntoView(gridLoader.SelectedItem);
            playCurrentSong();
        }
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            nextSong();
        }
        private void nextSong()
        {
            gridLoader.Focus();
            if (gridLoader.SelectedIndex + 1 > gridLoader.Items.Count - 1)
            { return; }
            gridLoader.SelectedIndex = gridLoader.SelectedIndex + 1;
            gridLoader.ScrollIntoView(gridLoader.SelectedItem);
            playCurrentSong();
        }
        #endregion

        #region Slider Controls for volume and Music
        private void volumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (volumeSlider.Value == 0)
            {
                //Symbol vico = Symbol.Mute;
                //SymbolIcon svico = new SymbolIcon(vico);
                //volButton.Icon = svico;
                player.IsMuted = true;
            }
            else
            {
                //Symbol vico = Symbol.Volume;
                //SymbolIcon svico = new SymbolIcon(vico);
                //volButton.Icon = svico;
                player.IsMuted = false;
                player.Volume = (volumeSlider.Value) / 100;
            }
        }
        private void SeekToMediaPosition_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int SliderValue = (int)SeekToMediaPosition.Value;
            player.Position = TimeSpan.FromSeconds(SliderValue);
            // Overloaded constructor takes the arguments days, hours, minutes, seconds, milliseconds.
            // Create a TimeSpan with miliseconds equal to the slider value.
            //TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue);
            //player.Position = ts;
        }
        #endregion

        private void DataGridRow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                playCurrentSong();
                gridLoader.SelectedIndex = gridLoader.SelectedIndex - 1;
            }
        }

        private void PrimaryWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var ctrl = (Window)sender;
            if (ctrl.IsLoaded)
            {
            }
        }

        private void gridLoader_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(e.NewSize.Height.Equals(451) && e.NewSize.Width.Equals(584))
            {

            }
        }
    }
    #region A Class That Contains Defination for DataGrid columns
    public class SongInfo
    {
        public String songName { get; set; }
        public String songPath { get; set; }
        public String songDirectory { get; set; }
        public String songLength { get; set; }
        public String songBitRate { get; set; }
        public String songDuration { get; set; }
    } 
    #endregion
}
