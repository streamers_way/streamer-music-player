﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Streamer_UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<User> paths = new List<User>();

        public MainWindow()
        {
            InitializeComponent();
            loadsongs();
        }
        private void loadsongs()
        {
        //    songContainer.Items.Clear();
            int count = 0;
            string rootDirectory = @"E:\Music";
            if (!Directory.Exists(rootDirectory))
            {
                MessageBoxResult dialogResult = MessageBox.Show("In your system you don't have any directory with this name " + rootDirectory + "\nDo you want to create directory tap on 'YES'");
                if (dialogResult == MessageBoxResult.Yes)
                {
                    Directory.CreateDirectory(rootDirectory);
                    MessageBox.Show("Place your songs in this folder " + rootDirectory.ToUpper() + " and enjoy\n\n Thanks for downloading");
                }
            }
            else
            {
                foreach (string files in Directory.GetFiles(rootDirectory, "*.mp3", SearchOption.AllDirectories))
                {
                    //paths.Add(new User() {Path=files });
                }
                FileInfo[] smFiles;
                // Get a list of all subdirectories  
                DirectoryInfo dinfo = new DirectoryInfo(rootDirectory);
                // Get a reference to each directory in that directory.
                DirectoryInfo[] diArr = dinfo.GetDirectories();
                // Display the names of the directories.
                count = 0;
                //gridLoader.AllowUserToAddRows = true;
                foreach (DirectoryInfo info in diArr)
                {
                    if (info.EnumerateDirectories().Count() > 0)
                    {
                        foreach (DirectoryInfo directoryInfo in info.EnumerateDirectories())
                        {
                            if (directoryInfo.EnumerateDirectories().Count() > 0)
                            {
                                foreach (DirectoryInfo directoryInfo1 in directoryInfo.EnumerateDirectories())
                                {
                                    if (directoryInfo1.EnumerateDirectories().Count() > 0)
                                    {
                                        foreach (DirectoryInfo directoryInfo2 in directoryInfo1.EnumerateDirectories())
                                        {
                                            smFiles = directoryInfo1.GetFiles("*.mp3");
                                            foreach (FileInfo fi in smFiles)
                                            {
                                                //     songContainer.Items.Add(Path.GetFileNameWithoutExtension(fi.Name));
                                                //gridLoader.Rows.Add("" + fi.Name + "", "" + fi.FullName + "");
                                                paths.Add(new User() { Name = fi.Name,Length="",Path=fi.FullName }) ;
                                            }
                                        }
                                        count++;
                                    }
                                    smFiles = directoryInfo1.GetFiles("*.mp3");
                                    foreach (FileInfo fi in smFiles)
                                    {
                                        //songContainer.Items.Add(Path.GetFileNameWithoutExtension(fi.Name));
                                        //gridLoader.Rows.Add("" + fi.Name + "", "" + fi.FullName + "");
                                        paths.Add(new User() { Name = fi.Name, Length = "", Path = fi.FullName });
                                    }
                                }
                                count++;
                            }
                            smFiles = directoryInfo.GetFiles("*.mp3");
                            foreach (FileInfo fi in smFiles)
                            {
                                // songContainer.Items.Add(Path.GetFileNameWithoutExtension(fi.Name));
                                //gridLoader.Rows.Add("" + fi.Name + "", "" + fi.FullName + "");
                                paths.Add(new User() { Name = fi.Name, Length = "", Path = fi.FullName });

                            }
                            count++;
                        }
                    }
                    smFiles = info.GetFiles("*.mp3");
                    foreach (FileInfo fi in smFiles)
                    {
                        // songContainer.Items.Add(Path.GetFileNameWithoutExtension(fi.Name));
                        //gridLoader.Rows.Add("" + fi.Name + "", "" + fi.FullName + "");
                        paths.Add(new User() { Name = fi.Name, Length = "", Path = fi.FullName });
                    }
                    count++;
                }
                gridLoader.ItemsSource = paths;

                //songContainer.Sorted = true;
                //songContainer.Focus();
                //songContainer.SelectedIndex = 0;
            }
        }

        private void HandleCheck(object sender, RoutedEventArgs e)
        {
            gridLoader.Visibility = Visibility.Visible;

        }
        private void HandleUnchecked(object sender, RoutedEventArgs e)
        {
            gridLoader.Visibility = Visibility.Hidden;
        }

        private void Window_AccessKeyPressed(object sender, AccessKeyPressedEventArgs e)
        {

        }

        private void MenuItemHelp_Click(object sender, RoutedEventArgs e)
        {
            StackPanel stackPanel = new StackPanel();
            stackPanel.Visibility = Visibility.Visible;
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MessageBoxResult boxResult = MessageBox.Show(sender.ToString());
        }
    }
    public class User
    {
        public string Name { get; set; }
        public string Length { get; set; }
        public string Path { get; set; }
    }
}
